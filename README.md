# SmartDeploy HTML & CSS Pattern Library

**!important**  
This folder is meant to be nested inside of /SmartDeploy.WebApp/SmartDeploy.WebApp so that all the project's stylesheets and scripts are accessible.

        __                      __
     .-'  `'.._...-----..._..-'`  '-.  
    /                                \  
    |  ,   ,'                '.   ,  |
     \  '-/                    \-'  /
      '._|                      |_.'
         |    /\   / \    /\    |
         |    \/   | |    \/    |
          \        \ /         /
           '.       ,        .'
             `'------------'`

## Getting Started with Development

1. Install [Node.js with npm](https://nodejs.org/en/) and [Gulp.js](http://gulpjs.com/)
2. Open command line and navigate to this directory
3. Run `npm install` to download all the required node modules
4. Run `npm install -g astrum` to get the pattern library tool
5. Run `gulp` to kick up the server and watch for file changes, or `gulp thetaskname` to run a single task

### Gulp Tasks

| Task Name  |  Task Purpose |
|---|---|
| `gulp serve` | Open & auto reload Astrum in the browser |
| `gulp importCSS` | Copy all required CSS files from the SD project into the /astrum/css folder |

## About Astrum

[This is Astrum](http://astrum.nodividestudio.com/). You can add, remove, or change settings for Astrum components from the command line pretty easily. Check out [Astrum's getting started guide](https://github.com/NoDivide/astrum#getting-started). Modify existing componenets by editing the files in the /astrum folder.

### Adding New CSS Assets

1. Add the file path in settings.json so that the gulp task will copy it into the astrum folder
2. Add the new file path to data.json so that astrum knows to load it in

## The Console Errors

tl;dr don't worry about it.

style.css tries to `@import` several style sheets, but Astrum isn't having any of it. So those files are being imported manually through the data.json file, and the errors stay because you can't remove the `@imports` from style.css when it needs to be re-copied into Astrum constantly.