    //# sourceURL=misc-scripts.js

/* Show and hide the password */
let pwdInput;
const pwdButton = $('#hide-show-pwd'),
    pwdButtonIcon = $('#hide-show-pwd .icon'),
    pwdButtonText = $('#hide-show-pwd .text');

// ! this fucntion needs to be called on the page where the script is called, like
// getPasswordInput($('input[id$="PasswordInput"]'));
const getPasswordInput = function (input) {
    pwdInput = input
}

const pwdToggle = function () {
    if (pwdButtonIcon.hasClass('fa-eye-slash')) {
        pwdButtonText.text('Show');
        pwdInput.attr('type', 'password');
    } else {
        pwdButtonText.text('Hide');
        pwdInput.attr('type', 'text');
    }
    pwdButtonIcon.toggleClass('fa-eye-slash').toggleClass('fa-eye');
};

var toggleKeys = [32, 37, 38, 39, 40]

// toggle for click & keypress
pwdButton.keydown(function (event) {
    // don't stop default behavoir if the user tabbed
    if (event.keyCode !== 9) {
        event.preventDefault();
    }
    // toggle the pwd for space bar and arrow keys
    toggleKeys.forEach(function(key) {
        if (key === event.keyCode) {
            pwdToggle();
        }
    })
});

pwdButton.click(function (event) {
    event.preventDefault();
    pwdToggle();
});