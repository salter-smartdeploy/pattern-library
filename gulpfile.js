'use strict'

const gulp = require('gulp'),
    settings = require('./settings.json'),
    svgSymbols = require('gulp-svg-symbols'),
    browserSync = require('browser-sync').create(),
    runSequence = require('run-sequence'),
    reload = browserSync.reload

/* 
 * Import CSS into astrum
 */
gulp.task('importCSS', () => {
    return gulp.src(settings.astrumCssSource)
        .pipe(gulp.dest(settings.astrumCssDest))
})

/* 
 * Run server and live reloading
 */
gulp.task('serve', () => {
    browserSync.init({
        server: './astrum'
    })
    gulp.watch(settings.customCss).on('change', reload)
    gulp.watch(settings.astrumPaths).on('change', reload)
})

/* 
 * Watch task 
 */
// might need this at some point to rebuild svgs, import css

/* 
 * Default tasks
 */
gulp.task('default', (callback) => {
    runSequence('importCSS', 'serve', callback)
});